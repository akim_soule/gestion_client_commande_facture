<?php

	$nom = (isset($client))?$client->nomClient: set_value('nom'); 
    $prenom =  (isset($client))?$client->prenomClient: set_value('prenom');
    $age = (isset($client))?$client->ageClient: set_value('number');
    $adresse = (isset($client))?$client->adresse: set_value('adresse');
    $ville = (isset($client))?$client->adresse: set_value('ville');
    $courriel = (isset($client))?$client->courrielClient: set_value('courriel');

    if (!isset($_SESSION['success'])) {
	echo "
<p>".validation_errors('<p style="background-color: red">', '</p>')."</p>

	<form action='' method='POST'>
		<table>
			<tr>
				<td><label>Nom :</label> </td>
				<td><input type='text' name='nom' value=".$nom."></td>
			</tr>
			<tr>
				<td><label>Prenom :</label> </td>
				<td><input type='text' name='prenom' value=".$prenom."></td>
			</tr>
			<tr>
				<td><label>Age :</label></td>
				<td><input type='number' name='number' value=".$age."></td>
			</tr>
			<tr>
				<td><label>Adresse :</label></td>
				<td><input type='text' name='adresse' value=".$adresse."></td>
			</tr>
			<tr>
				<td><label>Ville :</label></td>
				<td><SELECT name='ville' size='1'>
						<OPTION ".(($ville == 1)? "selected" : "").">Québec
						<OPTION ".(($ville == 2)? "selected" : "").">Montreal
						<OPTION ".(($ville == 3)? "selected" : "").">Gatineau
					</SELECT></td>
			</tr>
			<tr>
				<td><label>Courriel :</label></td>
				<td><input type='email' name='courriel' value=".$courriel."></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td><button name='modifier_client'>Modifier</button></td>
			</tr>
		</table>
	</form>";

}

  ?>