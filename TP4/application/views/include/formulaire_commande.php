<?php
$lesCodes = "<SELECT name = 'code1'>";
foreach ($listProduit as $produit) {
	$lesCodes .= '<OPTION value = '.$produit->codeArticle. '> '.$produit->codeArticle;
}
$lesCodes .= "</SELECT>";


$lesDescriptions = "<SELECT name = 'description1'>";
foreach ($listProduit as $produit) {
	$lesDescriptions .= '<OPTION value = '.$produit->description. '> '.$produit->description;
}
$lesDescriptions .= "</SELECT>";

$lesQuantite = "<SELECT name = 'quantite1' >";
foreach ($listProduit as $produit) {
	$lesQuantite .= '<OPTION value = '.$produit->quantite. '> '.$produit->quantite;
}
$lesQuantite .= "</SELECT>";

$lesPrix = "<SELECT name = 'prix1'>";
foreach ($listProduit as $produit) {
	$lesPrix .= '<OPTION value = '.$produit->prix. '> '.$produit->prix;
}
$lesPrix .= "</SELECT>";



$map = array();
$i = 0;
foreach ($listProduit as $produit) {
	$map[$i] = 
	array('code' => $produit->codeArticle,
		'description' => $produit->description, 
		'quantite' => $produit->prix, 
		'prix' => $produit->quantite
	);
	++$i;
}

$listDesClients = "<SELECT name = 'idClient'>";
foreach ($listClient as $client) {
	$listDesClients .= '<OPTION value = '.$client->idClient.'> '.$client->nomClient.", ".$client->prenomClient."\n".
	$client->courrielClient."\n".
	$client->adresse;
}
$listDesClients .= "</SELECT>";

?>

<?php

if (!isset($_SESSION["success"])) {
?>

<form action="<?php echo(base_url()."index.php/commande/passer")?>" method="POST">
	<?php

	echo "<p>".validation_errors('<p style="background-color: red">', '</p>')."</p>";

	?>

	<table  style="border-collapse: collapse;">
		<tr >
			<td colspan="7" style="border: black solid 1px;">
				Facture
			</td>
		</tr>
		<tr>
			<td colspan="7">
				
			</td>
		</tr>
		<tr>
			<td>
				
			</td>
			<td colspan="2">
				<?php
				echo $listDesClients;
				?>
			</td>
			<td colspan="2">
				
			</td>
			<td colspan="2">
				<table border="1" style="border-collapse: collapse;">
					<tr>
						<th>
							Numere de facture
						</th>
						<th>
							<?php
							echo "Information pour le numero de la facture";
							?>
						</th>
					</tr>
					<tr>
						<th>
							Date
						</th>
						<th>
							<textarea id="date" name = 'date'>
								<?php
									echo date("Y-m-d");
								?>
							</textarea>
						</th>
					</tr>
					<tr>
						<th>
							Somme due
						</th>
						<th>
							<div class="due" name = 'somme'></div>
						</th>
					</tr>
				</table>
				
			</td>
			
		</tr>
		<tr>
			<td colspan="6">
				
			</td>
		</tr>
		<tr style="border: black solid 1px;">
			<th>
				
			</th>
			<th>
				
			</th>
			<th style="border: black solid 1px;">
				Item
			</th>
			<th style="border: black solid 1px;">
				Description
			</th>
			<th style="border: black solid 1px;">
				Prix unitaire
			</th>
			<th style="border: black solid 1px;">
				Quantité
			</th>
			<th style="border: black solid 1px;">
				Prix total
			</th>
		</tr>
		<tr class="item-row">

			<td>
				<a class="delete" href="javascript:;" title="Remove row">X</a>
			</td>
			<td>
				<div class="numero">1</div>
			</td>
			<td>
				<?php
				echo $lesCodes;
				?>
			</td>
			<td>
				<?php
				echo $lesDescriptions;
				?>
			</td>
			<td>
				<textarea class="cost" name="cout1">0</textarea>
			</td>
			<td>
				<textarea class="qty" name="quantite1">0</textarea>
			</td>
			<td>
				<span class="price" name = "prix1">0.00</span>
			</td>

		</tr>

		<tr style="border: black solid 1px;">
			<td>
				
			</td>
			<td>
				
			</td>
			<td colspan="6" style="text-align: left;">
				<a id="addrow" href="javascript:;" title="Ajouter une commande">Add row</a> 
			</td>
		</tr>
		<tr style="border-left: black solid 1px;">
			<td>
				
			</td>
			<td colspan="4">
				
			</td>
			<td style="border: black solid 1px;">
				Sous total		
			</td>
			<td id="subtotal" style="border: black solid 1px;">
				0
			</td>
		</tr>
		<tr style="border-left: black solid 1px;">
			<td>
				
			</td>
			<td colspan="4">
				
			</td>
			<td style="border: black solid 1px;">
				Total	
			</td>
			<td id="total" style="border: black solid 1px;">
				0			
			</td>
		</tr>
		<tr style="border-left: black solid 1px;">
			<td>
				
			</td>
			<td colspan="4">

			</td>
			<td style="border: black solid 1px;">
				Montant paye
				
			</td>
			<td class="total-value" style="border: black solid 1px;">
				<textarea id="paid">$0.00</textarea>
			</td>
		</tr>
		<tr style="border-left: black solid 1px; border-bottom: black solid 1px;">
			<td>
				
			</td>
			<td colspan="4">
				
			</td>
			<td style="border: black solid 1px;">
				Balance dûe
				
			</td>
			<td class="due" style="border: black solid 1px;">
				0	
			</td>
		</tr>

		<tr>
			<td colspan="7" style="text-align: right;">
				<button>Soumettre</button>
			</td>
		</tr>

	</table>

</form>

<?php
}
?>



