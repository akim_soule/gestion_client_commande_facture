<?php
class Utilisateur_BD extends CI_Model  {
   private $count;
   protected $table;

  function __construct()
   {
      parent::__construct();
      $this->count = 0;
      $this->table = 'Utilisateur';
   }
   
  public function findAll()
   {
   	$query = $this->db->get($this->table);
   	if ($query->num_rows() > 0) {
   		return $query->result();
   					} else {
   		return false;
   		   	}
   	$query = $this->db->get($this->table,$start,$offset);
   	$this->count = $query->num_rows();
   	return $query->result();
   }
   
   function find($id)
   {       
       $query = $this->db->get_where($this->table, array('id' => $id));
       return $query->row();
   }

   function  getCount()
   {
       return $this->count;
   }

   function  getCountTable()
   {
       $query = $this->db->get($this->table);
       return $query->num_rows();
       
   }
   
   function add($data)
   {
      $this->db->insert($this->table, $data);
      return $this->db->insert_id();
   }

   function getMotDePasse ($nom)
   {
      $query = $this->db->query("SELECT motdepasse FROM Utilisateur where nom = '".$nom."'");

      return $query->row();
   }
   
   function update($id,$data)
   {
      $this->db->where('id', $id);
      $this->db->update($this->table, $data);
   }
   
   function delete($id)
   {
       $this->db->delete($this->table, array('id' => $id));
   }

   function isExists($nom)
   {
       $query = $this->db->get_where($this->table, array('nom' => $nom));
       return $query->num_rows() > 0  ;
   }
}

?>
