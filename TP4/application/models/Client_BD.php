<?php  


/**
 * 
 */
class Client_BD extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}
	
	public function estCeQueLeClientExiste($nomClient, $prenomClient)
	{
		$this->db->select('*');
		$this->db->from('Client');
		$this->db->where(array('nomClient' => $nomClient,
			'prenomClient' => $prenomClient));
		$query = $this->db->get();

		return $query->num_rows() > 0;
	}


	public function getAll()
	{
		$this->query('select * from Client');
		$query = $this->db->get();
		return $query->result();

	}

	public function enregistrerClient($data)
	{
		$this->db->insert('Client', $data);
	}

	public function findAll()
	{
		$query = $this->db->get('Client');
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
		
		$query = $this->db->get('Client',$start,$offset);
		$this->count = $query->num_rows();
		echo "09";
		return $query->result();
	}

	function find($id)
   {       
       $query = $this->db->get_where('Client', array('idClient' => $id));
       return $query->row();
   }

   function modifier($id, $data)
   {
   		$this->db->where('idClient', $id);
      	$this->db->update('Client', $data);
   }

   function delete($id)
   {
   		$sql2 = 'select idFacture from Facture where idClient = ?';

		$query = $this->db->query($sql2, array($id));

		$count = $query->num_rows();
		if ($count > 0) {
			return false;
		}else{
			$this->db->delete('Client', array('idClient' => $id));
			return true;
		}

       
   }

}
?>