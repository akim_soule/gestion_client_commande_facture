<?php


/**
 * 
 */
class Facture_BD extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	
	function enregistreFacture($idClient, $date, $arrayQuantite, $arrayCode)
	{
		

		$sql = 'insert into Facture (dateFacture, idClient) values (?, ?)';

		$this->db->query($sql, array($date, $idClient));

		$sql2 = 'select idFacture from Facture where dateFacture = ? and idClient = ?';

		$query = $this->db->query($sql2, array($date, $idClient));

		$row = $query->row();
		
		$idDeLaFacture = $row->idFacture;
		// echo $idDeLaFacture;
		// var_dump($arrayQuantite);
		// var_dump($arrayCode);

		$sql3 = "insert into lignefacture (idFacture, quantiteAchat, codeArticle) values (?, ?, ?)";

		for ($i=0; $i < count($arrayQuantite); $i++) { 
			$this->db->insert('ligneFacture',  
			array('idFacture' => $idDeLaFacture,
				'quantiteAchat' =>  $arrayQuantite[$i],
				'codeArticle' => $arrayCode[$i]));
		}

		// $this->db->query($sql3, array($idDeLaFacture, $arrayQuantite[$i], $arrayCode[$i]));

		// for ($i=1; $i < count($arrayQuantite) ; $i++) { 
		// 	$this->db->insert('ligneFacture',  
		// 	array('idFacture' => $idDeLaFacture,
		// 		'quantiteAchat' =>  $arrayQuantite[$i],
		// 		'codeArticle' => $arrayCode[$i]));
		// }
	

		

	}

	function lireCommandes($idClient_form)
	{
		$lesIDFactures  = array();

		$sql = 'select idFacture from facture where idClient = ? ';

		$query = $this->db->query($sql, array($idClient_form));

		$listeDesRowFactures = $query->result();

		for ($i=0; $i < count($listeDesRowFactures); $i++) { 
			$lesIDFactures[$i] =  $listeDesRowFactures[$i]->idFacture;
		}
		
		//var_dump($idClient_form);

		$resultat  = array();
		// var_dump($lesIDFactures);
		$sql2 = 'select * from lignefacture where idFacture = ';
		for ($i=0; $i < count($lesIDFactures); $i++) { 
			$sql2 .= ' ? ';
			if ($i+1 < count($lesIDFactures)) {
				$sql2 .= ' OR idFacture = ';
			}
		}

		$list = array();

		for ($i=0; $i < count($lesIDFactures); $i++) { 
			array_push($list, $lesIDFactures[$i]);
		}

		$result = $this->db->query($sql2, $list);
		
		return $result->result();

	}

}






?>