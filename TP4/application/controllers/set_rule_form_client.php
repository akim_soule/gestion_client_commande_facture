<?php



 $this->form_validation->set_rules('nom', 'Nom', 'required|min_length[3]|max_length[20]');
        $this->form_validation->set_rules('prenom', 'Prenom', 'required|min_length[3]|max_length[20]');
        $this->form_validation->set_rules('number', 'Number', 'required|alpha_numeric');
        $this->form_validation->set_rules('adresse', 'Adresse', 'required');
        $this->form_validation->set_rules('ville', 'Ville', 'required');
        $this->form_validation->set_rules('courriel', 'Courriel', 'required');