<?php

/**
 * 
 */
class Client extends CI_Controller
{
	
	function consulter()
	{
		session_start();
		unset($_SESSION['success']);
		unset($_SESSION['echec']);

		if (isset($_SESSION['nom'])) {
			$clientTotal = $this->Client_BD->findAll();
			$donnees = array('listClient' =>  $clientTotal,
							'titre' => 'Liste des clients');
			
			$this->load->view('include/gabarit.php', $donnees);
		}else{
			include 'mode_hors_connexion.php';
		}
		
	}
	function ajouter()
	{
		session_start();
		unset($_SESSION['success']);
		unset($_SESSION['echec']);

		if (isset($_SESSION['nom'])) {
			include 'set_rule_form_client.php';

			if ($this->form_validation->run() == TRUE){
				$utilitaire = new Utilitaire_modele();
				$nomClient = $utilitaire->transformNomPrenom($_POST['nom']);
				$prenomClient = $utilitaire->transformNomPrenom($_POST['prenom']);
				$ville = 0;
				$estCeQueLeClientExiste = $this->Client_BD->estCeQueLeClientExiste($nomClient, $prenomClient);

				if ($estCeQueLeClientExiste){
					$_SESSION['echec'] = "Le client ".$nomClient." ".$prenomClient." existe deja";
					$donnees['titre'] = "Ajouter client";
					$this->load->view('include/gabarit.php', $donnees);
				}else{
					
					switch ($_POST['ville']) {
						case 'Québec':
							$ville = 1;
							break;
						case 'Montreal':
							$ville = 2;
							break;
						case 'Gatineau':
							$ville = 3;
							break;
					}

					$data = array('nomClient' => $nomClient,
								'prenomClient' => $prenomClient,
								'ageClient' => $_POST['number'],
								'courrielClient' => $_POST['courriel'],
								'adresse' => $_POST['adresse'],
								'idVille' => $ville);
					$this->Client_BD->enregistrerClient($data);
					$_SESSION['success'] = "Le client ".$nomClient." ".$prenomClient." a été ajouté avec succes";

					$donnees['titre'] = "Ajouter client";
					$this->load->view('include/gabarit', $donnees);
				}
			
			}else{
				$donnees['titre'] = "Ajouter client";
				$this->load->view('include/gabarit', $donnees);
			}
		}else{
			$_SESSION['echec'] = "Vous n'etes pas connecte !!";
			$donnees['titre'] = "Connection";
			$this->load->view('include/gabarit', $donnees);
		}

		

	}

	function modifier($id)
	{
		session_start();
		unset($_SESSION['success']);
		unset($_SESSION['echec']);

		if (isset($_SESSION['nom'])) {
			$donnees['client'] = $this->Client_BD->find($id);
			$donnees['titre'] = 'Modifier client';

	
		include 'set_rule_form_client.php';

        if ($this->form_validation->run() == TRUE){
				$utilitaire = new Utilitaire_modele();
				$nomClient = $utilitaire->transformNomPrenom($_POST['nom']);
				$prenomClient = $utilitaire->transformNomPrenom($_POST['prenom']);
				$ville = 0;

					switch ($_POST['ville']) {
						case 'Québec':
							$ville = 1;
							break;
						case 'Montreal':
							$ville = 2;
							break;
						case 'Gatineau':
							$ville = 3;
							break;
					}

					$data = array('nomClient' => $nomClient,
								'prenomClient' => $prenomClient,
								'ageClient' => $_POST['number'],
								'courrielClient' => $_POST['courriel'],
								'adresse' => $_POST['adresse'],
								'idVille' => $ville);
					$this->Client_BD->modifier($id, $data);

					$_SESSION['success'] = "Le client ".$nomClient." ".$prenomClient." a été modifié avec succes";

					$donnees['titre'] = 'Modifier client';


					$this->load->view('include/gabarit.php', $donnees);
				
			
			}else{
				$this->load->view('include/gabarit.php', $donnees);
			}
		}else{
			include 'mode_hors_connexion.php';
		}

		
	}

	function supprimer($id)
	{
		session_start();
		unset($_SESSION['success']);
		unset($_SESSION['echec']);

		if (isset($_SESSION['nom'])) {
			$boul = $this->Client_BD->delete($id);
			if ($boul) {
				$_SESSION['success'] = "Le client a été supprimé avec succes";
				
			}else{
				$_SESSION['echec'] = "Impossible de supprimer le client";
			}

			$clientTotal = $this->Client_BD->findAll();
				$donnees = array('listClient' =>  $clientTotal,
							'titre' => 'Liste des clients');
			
				$this->load->view('include/gabarit', $donnees);
			

			
		}else{
			include 'mode_hors_connexion.php';
		}

		
	}
}


?>