<?php 


/**
 * 
 */
class Commande extends CI_Controller
{
	
	function passer()
	{
		session_start();
		unset($_SESSION['success']);
		unset($_SESSION['echec']);

		if (isset($_SESSION['nom'])) {

			for ($i=1; $i < 10; $i++) { 
				if (isset($_POST['cout'.$i])) {
					$this->form_validation->set_rules('cout'.$i, 'Le cout '.$i, 'required|alpha_numeric|greater_than[0]');
				}
				if (isset($_POST['quantite'.$i])) {
					$this->form_validation->set_rules('quantite'.$i, 'La quantite '.$i, 'required|alpha_numeric|greater_than[0]');
				}

			}

			if ($this->form_validation->run() == TRUE){

				//echo $_POST['idClient'];
				//echo $_POST['date'];
				
				$arrayQuantite = array();
				$arrayCode = array();

				for ($i=0; $i < 10; $i++) { 
					if (isset($_POST['quantite'.$i]) && isset($_POST['code'.$i])) {
						array_push($arrayQuantite, $_POST['quantite'.$i]);
						array_push($arrayCode, $_POST['code'.$i]);
					}

				}


				$donnees = array('idClient' => $_POST['idClient'],
								'date' => $_POST['date']);

				$this->Facture_BD->enregistreFacture($_POST['idClient'], $_POST['date'], $arrayQuantite, $arrayCode);



				$_SESSION['success'] = "La facture a ete bien ajoutee";
				$produitTotal = $this->Produit_BD->findAll();
				$clientTotal = $this->Client_BD->findAll();

				$donnees = array('listProduit' =>  $produitTotal,
				'listClient' => $clientTotal,
						'aAjouter' => '',
						'titre' => 'Passer une comamde');
				$this->load->view('include/gabarit', $donnees);

			}else{
				$produitTotal = $this->Produit_BD->findAll();
				$clientTotal = $this->Client_BD->findAll();

				$donnees = array('listProduit' =>  $produitTotal,
				'listClient' => $clientTotal,
						'aAjouter' => '',
						'titre' => 'Passer une comamde');
				$this->load->view('include/gabarit', $donnees);
			}
			


			
		}else{
			include 'mode_hors_connexion.php';
		}
		
	}
}
 ?>