<?php


/**
 * 
 */
class Produit extends CI_Controller
{
	
	function consulter()
	{
		session_start();
		unset($_SESSION['success']);
		unset($_SESSION['echec']);

		if (isset($_SESSION['nom'])) {
			if (isset($_POST['rechercher_produit'])) {
				$motcle = "";
				$categorie = 0;
				$tri = "";
				$ordre;
				$data = array();
				if (isset($_POST['motcle']) and $_POST['motcle'] != ""){
					$motcle = $_POST['motcle'];
					$data['motcle'] = $motcle;
				}
				
				echo $_POST['categorie'];
				if (isset($_POST['categorie']) and $_POST['categorie'] != "") {
					switch ($_POST['categorie']) {
						case 'Informatique':
						$categorie = 1;
						break;
						case 'Livre':
						$categorie = 2;
						break;
						case 'Logiciel':
						$categorie = 3;
						break;
					}
					$data['categorie'] = $categorie;
				}
				
				
				
				if (isset($_POST['tri']) and $_POST['tri'] != "") {
					$data['tri'] = $_POST['tri'];
				}
				
				
				if (isset($_POST['ordre']) and $_POST['ordre'] != ""){
					switch ($_POST['ordre']) {
						case 'croissant':
						$tri = "ASC";
						break;
						case 'decroissant':
						$tri = "DESC";
						break;
					}
					
					$data['ordre'] = $tri;
				}

				$produitTotal = $this->Produit_BD->appliquerFiltre($data);
				var_dump($produitTotal);
				$donnees = array('listProduit' =>  $produitTotal,
					'titre' => 'Liste des produits');

				$this->load->view('include/gabarit', $donnees);

			}else{
				$produitTotal = $this->Produit_BD->findAll();
				$donnees = array('listProduit' =>  $produitTotal,
					'titre' => 'Liste des produits');

				$this->load->view('include/gabarit', $donnees);
			}
			
		}else{
			include 'mode_hors_connexion.php';
		}

		
	}


}