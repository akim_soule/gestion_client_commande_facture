<?php


/**
 * 
 */
class Facture extends CI_Controller
{
	function afficher()
	{
		session_start();
		unset($_SESSION['success']);
		unset($_SESSION['echec']);

		if (isset($_SESSION['nom'])) {
			$clientTotal = $this->Client_BD->findAll();
			
			$donnees = array('listClient' =>  $clientTotal,
				'titre' => 'Afficher une facture');

			$this->form_validation->set_rules('idClient', 'IDCLIENT', 'required');

			if (isset($_POST['soumettre_nom_client']) && $this->form_validation->run() == TRUE){

				$lesCommandes = $this->Facture_BD->lireCommandes($_POST['idClient']);
				$clientTrouve = $this->Client_BD->find($_POST['idClient']);

				$donnees = array('listClient' =>  $clientTotal,
								'titre' => 'Afficher une facture', 
								'lesCommandes' => $lesCommandes,
								'clientTrouve' => $clientTrouve);

			}

			$this->load->view('include/gabarit.php', $donnees);

		}else{
			include 'mode_hors_connexion.php';
		}
	}
}