<?php 


/**
* 
*/
class Connection extends CI_Controller
{

	public function index()
	{
		session_start();
		unset($_SESSION['success']);
		unset($_SESSION['echec']);
		if (isset($_SESSION['nom'])){
			$donnees['titre'] = 'Acceuil';
			$this->load->view('include/gabarit', $donnees);
		}else {
			$this->form_validation->set_rules('username', 'Username', 'required');
			$this->form_validation->set_rules('password', 'Password', 'required|min_length[5]');

			if ($this->form_validation->run() == TRUE){
				$estCeLeUseExiste = $this->Utilisateur_BD->isExists($_POST['username']);

				if ($estCeLeUseExiste){

					$motDePasseDeLaBD = $this->Utilisateur_BD->getMotDePasse($_POST['username'])->motdepasse;

					if (password_verify($_POST['password'], $motDePasseDeLaBD)){
						$_SESSION['nom'] = $_POST['username'];
						$donnees['titre'] = 'Acceuil';
						$this->load->view('include/gabarit', $donnees);

					}else{
						$_SESSION['echec'] = 'Mauvais utilisateur ou mot de passe';
						$donnees['titre'] = 'Connection';
						$this->load->view('include/gabarit', $donnees);
					}

				}else{

					$_SESSION['echec'] = 'Mauvais utilisateur ou mot de passe';
					$donnees['titre'] = 'Connection';
					$this->load->view('include/gabarit', $donnees);

				}

			}else{
				$donnees['titre'] = 'Connection';
				$this->load->view('include/gabarit', $donnees);

			}
		}

		

	}

}

