<?php



/**
 * 
 */
class Inscription extends CI_Controller
{
	// private $modele_bd;

	// function __construct()
	// {
	// 	$this->modele_bd = $this->modele_bd;
	// }

	public function index()
	{
		session_start();
		unset($_SESSION['success']);
		unset($_SESSION['echec']);
		if (isset($_SESSION['nom'])){
			$donnees['titre'] = 'Acceuil';
					$this->load->view('include/gabarit', $donnees);
		}else{
			$this->form_validation->set_rules('username', 'Username', 'required');
			$this->form_validation->set_rules('password', 'Password', 'required|min_length[5]');
			$this->form_validation->set_rules('password2', 'Confirm Password', 'required|min_length[5]|matches[password]');
			
			if ($this->form_validation->run() == TRUE){
				
				$boolean = $this->Utilisateur_BD->isExists($_POST['username']);

				if ($boolean){
					$_SESSION['echec'] = 'Mauvais utilisateur ou mot de passe';
					$donnees['titre'] = 'Inscription';
					$this->load->view('include/gabarit', $donnees);

				}else{
					$dataBD = array('nom' => $_POST['username'],
							  'motdepasse' => password_hash($_POST['password'], PASSWORD_DEFAULT)
									);
					$this->Utilisateur_BD->add($dataBD);
					$_SESSION['success'] = 'Yous account has been registred. You can login now';
					$donnees['titre'] = 'Inscription';
					$this->load->view('include/gabarit', $donnees);
				}
			
			}else{
				$donnees['titre'] = 'Inscription';
				$this->load->view('include/gabarit', $donnees);
			}
		}
		
	}
}

  ?>